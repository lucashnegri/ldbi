/*
    This file is part of ldbi.

    ldbi is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ldbi is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with ldbi.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2008 - 2014 Lucas Hermann Negri
*/

#include "postgres.h"

static void priv_notice_processor(void* param, const char* notice)
{
    /* Do nothing */
}

static int priv_conn_tostring(lua_State* L)
{
    Connection* obj = lua_touserdata(L, 1);

    char name[41];
    snprintf(name, 40, "Postgres Connection: %p", obj->pointer);
    lua_pushstring(L, name);

    return 1;
}

static int priv_cursor_tostring(lua_State* L)
{
    Cursor* obj = lua_touserdata(L, 1);

    char name[41];
    snprintf(name, 40, "Postgres Cursor: %p", obj->pointer);
    lua_pushstring(L, name);

    return 1;
}

static void priv_conn_push(lua_State* L, PGconn* ptr)
{
    #ifdef IDEBUG
    fprintf(stderr, "%s\n", "Creating a Postgres Connection!");
    #endif

    /* Disable the notices */
    PQsetNoticeProcessor(ptr, priv_notice_processor, NULL);

    Connection* obj = lua_newuserdata(L, sizeof(Connection));
    obj->pointer = ptr;
    obj->need_unref = true;

    /* Set the object metatable */
    luaL_getmetatable(L, "__mtPGCoNn");
    lua_setmetatable(L, -2);
}

static void priv_cursor_push(lua_State* L, PGresult* res)
{
    #ifdef IDEBUG
    fprintf(stderr, "%s\n", "Creating a Postgres Cursor!");
    #endif

    Cursor* obj = lua_newuserdata(L, sizeof(Cursor));
    obj->pointer = res;
    obj->need_unref = true;
    obj->n_rows = PQntuples(res);
    obj->n_cols = PQnfields(res);
    obj->row = 0;

    /* Set the object metatable */
    luaL_getmetatable(L, "__mtPGCuRsOr");
    lua_setmetatable(L, -2);
}

static void close_cursor(Cursor* cursor)
{
    #ifdef IDEBUG
    fprintf(stderr, "%s\n", "Closing a Postgres Cursor!");
    #endif

    if(cursor->need_unref)
    {
        cursor->need_unref = false;
        PQclear(cursor->pointer);
    }
}

int ldbi_env_connect(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TSTRING);

    PGconn* ptr = PQconnectdb(lua_tostring(L, 1));

    /* Check the status */
    if(PQstatus(ptr) != CONNECTION_OK)
    {
        lua_pushnil(L);
        lua_pushstring(L, "08004"); /* Client unable to connect */
        lua_pushstring(L, PQerrorMessage(ptr));
        PQfinish(ptr);
        return 3;
    }

    /* OK, push it */
    priv_conn_push(L, ptr);
    return 1;
}

int ldbi_conn_close(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Connection* obj = lua_touserdata(L, 1);

    if(obj->need_unref)
    {
        obj->need_unref = false;
        PQfinish(obj->pointer);
    }
    
    #ifdef IDEBUG
    fprintf(stderr, "%s\n", "Closing a Postgres Connection!");
    #endif

    return 0;
}

int ldbi_conn_escape(lua_State *L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);

    size_t len;
    const char* from = luaL_checklstring(L, 2, &len);
    char* to = malloc((len * 2 + 1) * sizeof(char));
    int error;

    Connection* obj = lua_touserdata(L, 1);
    PQescapeStringConn(obj->pointer, to, from, len, &error);

    if(!error)
        lua_pushstring(L, to);
    else
        lua_pushnil(L);

    free(to);

    return 1;
}

int ldbi_conn_execute(lua_State *L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    luaL_checktype(L, 2, LUA_TSTRING);

    Connection* obj = lua_touserdata(L, 1);
    const char* sql = lua_tostring(L, 2);
    int param_count = 0;

    if(lua_isnumber(L, 3))
    {
        luaL_checktype(L, 3, LUA_TNUMBER);
        luaL_checktype(L, 4, LUA_TTABLE);
        param_count = lua_tointeger(L, 3);
    }

    const char** params = NULL;

    if(param_count > 0)
    {
        /* Allocate the array */
        params = malloc(sizeof(char*) * param_count);

        /* Get the params */
        int i;
        for(i = 1; i <= param_count; ++i)
        {
            lua_pushnumber(L, i);
            lua_rawget(L, 4);

            if(lua_isboolean(L, -1))
                params[i - 1] = bool_rep[lua_toboolean(L, -1) != 0 ? 1 : 0];
            else
                params[i - 1] = lua_tostring(L, -1);

            lua_pop(L, 1);
        }
    }

    /* Do the query */
    PGresult* res = PQexecParams(obj->pointer, sql, param_count, NULL,
        params, NULL, NULL, 0);

    if(param_count > 0)
    {
        /* Free the parameters */
        free(params);
    }

    /* Check the results */
    ExecStatusType status = PQresultStatus(res);

    if(status == PGRES_COMMAND_OK)
    {
        /* OK, without tuples */
        lua_pushinteger(L, atoi(PQcmdTuples(res)));
        PQclear(res);
        return 1;
    }
    else if(status == PGRES_TUPLES_OK)
    {
        /* OK, with tuples */
        priv_cursor_push(L, res);
        return 1;
    }
    else
    {
        /* Error */
        lua_pushnil(L);
        lua_pushstring(L, PQresultErrorField(res, PG_DIAG_SQLSTATE));
        lua_pushstring(L, PQresultErrorMessage(res));
        PQclear(res);
        return 3;
    }
}

int ldbi_conn_version(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Connection* obj = lua_touserdata(L, 1);
    char buffer[21];
    snprintf(buffer, 20, "%i", PQserverVersion(obj->pointer));

    lua_pushstring(L, LIB_VERSION);
    lua_pushstring(L, buffer);

    return 2;
}

int ldbi_conn_file_import(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    luaL_checktype(L, 2, LUA_TSTRING);
    
    Connection* conn = lua_touserdata(L, 1);
    Oid id = lo_import(conn->pointer, lua_tostring(L, 2));
    lua_pushinteger(L, id);
    return 1;
}

int ldbi_conn_file_export(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    luaL_checktype(L, 2, LUA_TNUMBER);
    luaL_checktype(L, 3, LUA_TSTRING);
    
    Connection* conn = lua_touserdata(L, 1);
    int res = lo_export(conn->pointer, lua_tointeger(L, 2), lua_tostring(L, 3));
    lua_pushinteger(L, res);
    
    return 1;
}

int ldbi_conn_file_delete(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    luaL_checktype(L, 2, LUA_TNUMBER);
    
    Connection* conn = lua_touserdata(L, 1);
    int res = lo_unlink(conn->pointer, lua_tointeger(L, 2));
    lua_pushinteger(L, res);
    
    return 1;
}

int ldbi_cursor_fetch(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    luaL_checktype(L, 2, LUA_TTABLE);

    Cursor* obj = lua_touserdata(L, 1);

    /* There are more rows? */
    if(obj->row < obj->n_rows)
    {
        /* Push all the fields */
        int i;
        for(i = 0; i < obj->n_cols; ++i)
        {
            if(PQgetisnull(obj->pointer, obj->row, i))
                lua_pushnil(L);
            else
                lua_pushstring(L, PQgetvalue(obj->pointer, obj->row, i));

            lua_rawseti(L, 2, i + 1);
        }
        
        /* OK, next row */
        ++obj->row;
    }
    else
    {
        /* close the cursor */
        
        /* Consistency */
        lua_pushnil(L);
    }

    return 1;
}

int ldbi_cursor_get_count(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Cursor* obj = lua_touserdata(L, 1);
    lua_pushinteger(L, obj->n_cols);
    lua_pushinteger(L, obj->n_rows);

    return 2;
}

int ldbi_cursor_get_column_types(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    luaL_checktype(L, 2, LUA_TUSERDATA);
    Cursor* obj1 = lua_touserdata(L, 1);
    Connection* obj2 = lua_touserdata(L, 2);

    /* To help the type query */
    char stmt[100];
    Oid id;

    lua_newtable(L);

    /* Query the types by the Oid */
    int i;
    for(i = 0; i < obj1->n_cols; ++i)
    {
        bool ok = false;
        id = PQftype(obj1->pointer, i);
        sprintf(stmt, "select typname from pg_type where oid = %d", id);
        PGresult *res = PQexec(obj2->pointer, stmt);

        if(PQresultStatus(res) == PGRES_TUPLES_OK)
        {
            if(PQntuples(res) > 0)
            {
                lua_pushstring(L, PQgetvalue(res, 0, 0));
                ok = true;
            }
        }

        if(!ok)
            lua_pushstring(L, "undefined");

        lua_rawseti(L, 3, i + 1);
        PQclear(res);
    }

    return 1;
}

int ldbi_cursor_get_column_names(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Cursor* obj = lua_touserdata(L, 1);

    lua_newtable(L);

    int i;
    for(i = 0; i < obj->n_cols; ++i)
    {
        lua_pushstring(L, PQfname(obj->pointer, i));
        lua_rawseti(L, 2, i + 1);
    }

    return 1;
}

int ldbi_cursor_close(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Cursor* obj = lua_touserdata(L, 1);
    close_cursor(obj);

    return 0;
}

/* Interface */

static const struct luaL_Reg env [] =
{
    {"connect", ldbi_env_connect},
    {NULL, NULL}
};

static const struct luaL_Reg conn [] =
{
    {"execute", ldbi_conn_execute},
    {"escape", ldbi_conn_escape},
    {"version", ldbi_conn_version},
    {"fileImport", ldbi_conn_file_import},
    {"fileExport", ldbi_conn_file_export},
    {"fileDelete", ldbi_conn_file_delete},
    {"close", ldbi_conn_close},
    {NULL, NULL}
};

static const struct luaL_Reg cursor [] =
{
    {"fetch", ldbi_cursor_fetch},
    {"getCount", ldbi_cursor_get_count},
    {"getColumnNames", ldbi_cursor_get_column_names},
    {"getColumnTypes", ldbi_cursor_get_column_types},
    {"close", ldbi_cursor_close},
    {NULL, NULL}
};

int luaopen_ldbi__postgres(lua_State *L)
{
    /* Register the metatables */
    ldbi_push_metatable(L, "__mtPGCoNn", conn, ldbi_conn_close, priv_conn_tostring);
    ldbi_push_metatable(L, "__mtPGCuRsOr", cursor, ldbi_cursor_close, priv_cursor_tostring);
    
    /* Register the basic functions */
    luaL_setfuncs(L, env, 0);

    return 1;
}
