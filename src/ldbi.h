/*
    This file is part of ldbi.

    ldbi is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ldbi is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with ldbi.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2008 - 2014 Lucas Hermann Negri
*/

#ifndef LDBI_INTERFACE
#define LDBI_INTERFACE

/* C */
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Lua */
#include <lua.h>
#include <lauxlib.h>

#define LIB_NAME "ldbi"
#define LIB_VERSION "0.5"
#define LDBI_NPAR = 6

/* Aux */

/**
 * Resgisters a metatable.
 */
void ldbi_push_metatable(lua_State *L, const char *name, const luaL_Reg *methods,
    lua_CFunction gc, lua_CFunction tostring);

/* Env */

/**
 * Connects to the database.
 */
int ldbi_env_connect(lua_State* L);

/* Con */

/**
 * Gets the library (binding and backend) version.
 */
int ldbi_conn_version(lua_State* L);

/**
 * Closes the connection.
 */
int ldbi_conn_close(lua_State* L);

/**
 * Execute a statement.
 */
int ldbi_conn_execute(lua_State* L);

/**
 * Escapes a string to prevent SQL injection.
 */
int ldbi_conn_escape(lua_State* L);

/* Cursor */

/**
 * Closes the cursor.
 */
int ldbi_cursor_close(lua_State* L);

/**
 * Fetchs a row from the cursor.
 */
int ldbi_cursor_fetch(lua_State* L);

/**
 * Returns the number of columns and rows of the cursor.
 */
int ldbi_cursor_get_count(lua_State* L);

/**
 * Gets the names of the cursor columns.
 */
int ldbi_cursor_get_column_names(lua_State* L);

/**
 * Gets the types of the cursor columns.
 */
int ldbi_cursor_get_column_types(lua_State* L);

#if !defined LUA_VERSION_NUM || LUA_VERSION_NUM==501
void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup);
#endif

#if !defined(LUA_VERSION_NUM) || LUA_VERSION_NUM < 501
# define lua_rawlen lua_strlen
#elif LUA_VERSION_NUM == 501
# define lua_rawlen lua_objlen
#endif

#endif
