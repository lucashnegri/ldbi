/*
    This file is part of ldbi.

    ldbi is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ldbi is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with ldbi.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2008 - 2014 Lucas Hermann Negri
*/

#ifndef LDBI_SQLITE
#define LDBI_SQLITE

#include <sqlite3.h>
#include "ldbi.h"

/* Connection object */
typedef struct
{
    sqlite3* pointer;
    bool need_unref;
} Connection;

/* Cursor object */
typedef struct
{
    sqlite3_stmt* pointer;
    bool need_unref;
    int n_cols;
} Cursor;

#endif
