/*
    This file is part of ldbi.

    ldbi is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ldbi is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with ldbi.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2008 - 2014 Lucas Hermann Negri
*/

#include "ldbi.h"

void ldbi_push_metatable(lua_State* L, const char* name, const luaL_Reg* methods,
    lua_CFunction gc, lua_CFunction tostring)
{
    luaL_newmetatable(L, name);

    /* Methods */
    luaL_setfuncs(L, methods, 0);

    /* __gc */
    lua_pushstring(L, "__gc");
    lua_pushcfunction(L, gc);
    lua_settable (L, -3);

    /* __tostring */
    lua_pushstring(L, "__tostring");
    lua_pushcfunction(L, tostring);
    lua_settable (L, -3);

    /* __index */
    lua_pushstring(L, "__index");
    lua_pushvalue(L, -2);
    lua_settable (L, -3);
}

#if !defined LUA_VERSION_NUM || LUA_VERSION_NUM==501
void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup)
{
    luaL_checkstack(L, nup+1, "too many upvalues");
    for (; l->name != NULL; ++l)
    {
        int i;
        lua_pushstring(L, l->name);
        
        for (i = 0; i < nup; i++)
            lua_pushvalue(L, -(nup+1));
        
        lua_pushcclosure(L, l->func, nup);
        lua_settable(L, -(nup + 3));
    }
    
    lua_pop(L, nup);
}
#endif
