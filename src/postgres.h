/*
    This file is part of ldbi.

    ldbi is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ldbi is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with ldbi.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (C) 2008 - 2014 Lucas Hermann Negri
*/

#ifndef LDBI_POSTGRES
#define LDBI_POSTGRES

#include <libpq-fe.h>
#include "ldbi.h"

/* Boolean representation */
static const char* bool_rep[] = {
    "f", "t"
};

/* Connection object */
typedef struct
{
    PGconn* pointer;
    bool need_unref;
} Connection;

/* Cursor object */
typedef struct
{
    PGresult* pointer;
    int n_rows;
    int n_cols;
    int row;
    bool need_unref;
} Cursor;

#endif
