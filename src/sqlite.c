#include "sqlite.h"

static void priv_conn_push(lua_State* L, sqlite3* ptr)
{
    #ifdef IDEBUG
    fprintf(stderr, "%s\n", "Creating a SQLite3 Connection!");
    #endif
    
    Connection* obj = lua_newuserdata(L, sizeof(Connection));
    obj->pointer = ptr;
    obj->need_unref = true;

    /* Set the object metatable */
    luaL_getmetatable(L, "__mtSqLiTeCoNn");
    lua_setmetatable(L, -2);
}

static int priv_conn_tostring(lua_State* L)
{
    Connection* obj = lua_touserdata(L, 1);

    char name[41];
    snprintf(name, 40, "SQLite3 Connection: %p", obj->pointer);
    lua_pushstring(L, name);

    return 1;
}

static void priv_cursor_push(lua_State* L, sqlite3_stmt* res, int cols)
{
    #ifdef IDEBUG
    fprintf(stderr, "%s\n", "Creating a SQLite3 Cursor!");
    #endif

    Cursor* obj = lua_newuserdata(L, sizeof(Cursor));
    obj->pointer = res;
    obj->need_unref = true;
    obj->n_cols = cols;

    /* Set the object metatable */
    luaL_getmetatable(L, "__mtSqLiTeCuRsOr");
    lua_setmetatable(L, -2);
}

static int priv_cursor_tostring(lua_State* L)
{
    Cursor* obj = lua_touserdata(L, 1);

    char name[41];
    snprintf(name, 40, "SQLite3 Cursor: %p", obj->pointer);
    lua_pushstring(L, name);

    return 1;
}

static void close_cursor(Cursor* cursor)
{
    #ifdef IDEBUG
    fprintf(stderr, "%s\n", "Closing a SQLite3 Cursor!");
    #endif
    
    if(cursor->need_unref)
    {
        cursor->need_unref = false;
        sqlite3_finalize(cursor->pointer);
    }
}

int ldbi_env_connect(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TTABLE);
    lua_pushstring(L, "dbname");
    lua_rawget(L, -2);
    
    sqlite3* ptr;
    int status = sqlite3_open(lua_tostring(L, -1), &ptr);
    
    if(status != SQLITE_OK)
    {
        lua_pushnil(L);
        lua_pushinteger(L, sqlite3_errcode(ptr));
        lua_pushstring(L, sqlite3_errmsg(ptr));
        sqlite3_close(ptr);
        
        return 3;
    }
    
    priv_conn_push(L, ptr);
    
    return 1;
}

int ldbi_conn_execute(lua_State *L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    luaL_checktype(L, 2, LUA_TSTRING);

    Connection* obj = lua_touserdata(L, 1);
    int param_count = 0;

    if(lua_isnumber(L, 3))
    {
        luaL_checktype(L, 3, LUA_TNUMBER);
        luaL_checktype(L, 4, LUA_TTABLE);
        param_count = lua_tointeger(L, 3);
    }
    
    /* Get the stmt */
    sqlite3_stmt* stmt;
    const char* tail;
    int res = sqlite3_prepare_v2(obj->pointer, lua_tostring(L, 2), lua_rawlen(L, 2),
        &stmt, &tail);
        
    if(res != SQLITE_OK)
    {
        lua_pushnil(L);
        lua_pushinteger(L, res);
        lua_pushstring(L, sqlite3_errmsg(obj->pointer));
        return 3;   
    }
    
    /* Bind the values */
    int i;
    for(i = 1; i <= param_count; ++i)
    {
        lua_pushinteger(L, i);
        lua_rawget(L, 4);
        
        switch(lua_type(L, -1))
        {
            case LUA_TNUMBER:
            {
                #ifdef IDEBUG
                fprintf(stderr, "Handling as number\n");
                #endif
                
                sqlite3_bind_double(stmt, i, lua_tonumber(L, -1)); 
                break;
            }
            case LUA_TBOOLEAN:
            {
                #ifdef IDEBUG
                fprintf(stderr, "Handling as boolean\n");
                #endif
                
                sqlite3_bind_text(stmt, i, lua_toboolean(L, -1) ? "t" : "f", 2, SQLITE_TRANSIENT); 
                break;
            }
            case LUA_TNIL:
            {
                #ifdef IDEBUG
                fprintf(stderr, "Handling as NULL\n");
                #endif
                
                sqlite3_bind_null(stmt, i);
                break;
            }
            case LUA_TTABLE:
            {
                #ifdef IDEBUG
                fprintf(stderr, "Handling as binary\n");
                #endif
                
                lua_pushinteger(L, 1);
                lua_rawget(L, -2);
                size_t s;
                const char* data = lua_tolstring(L, -1, &s);
                lua_pop(L, 1);
                sqlite3_bind_blob(stmt, i, data, s, SQLITE_TRANSIENT);
                break;
            }
            default:
            {
                #ifdef IDEBUG
                fprintf(stderr, "Handling as text\n");
                #endif
                
                size_t s;
                const char* data = lua_tolstring(L, -1, &s);
                sqlite3_bind_text(stmt, i, data, s, SQLITE_TRANSIENT);
            }
        }
    }
    
    /* Get the status */
    res = sqlite3_step(stmt);
    int cols = sqlite3_column_count(stmt);
    
    if( (res == SQLITE_ROW) || ( (res == SQLITE_DONE) &&  cols > 0) )
    {
        /* Reset and retun a cursor */
        sqlite3_reset(stmt);
        priv_cursor_push(L, stmt, cols);
        
        return 1;
    }
    
    if(res == SQLITE_DONE)
    {
        /* Query OK, but no returned rows */
        sqlite3_finalize(stmt);
        lua_pushnumber(L, sqlite3_changes(obj->pointer));
        
        return 1;
    }
    
    /* Error */
    lua_pushnil(L);
    lua_pushinteger(L, sqlite3_errcode(obj->pointer));
    lua_pushstring(L, sqlite3_errmsg(obj->pointer));
    sqlite3_finalize(stmt);
    
    return 3;
}

int ldbi_conn_version(lua_State* L)
{
    char buffer[21];
    snprintf(buffer, 20, "%i", sqlite3_libversion_number());
    
    lua_pushstring(L, LIB_VERSION);
    lua_pushstring(L, buffer);

    return 2;
}

int ldbi_conn_escape(lua_State *L)
{
    /* No current need for the self param */
    luaL_checktype(L, 2, LUA_TSTRING);
    char* to = sqlite3_mprintf("%q", lua_tostring(L, 2));
    
    if(to != NULL)
    {
        lua_pushstring(L, to);
        sqlite3_free(to);
    }
    else
    {
        /* Consistency */
        lua_pushnil(L); 
    }
    
    return 1;
}

int ldbi_conn_close(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Connection* obj = lua_touserdata(L, 1);

    if(obj->need_unref)
    {
        obj->need_unref = false;
        sqlite3_close(obj->pointer);
    }
    
    #ifdef IDEBUG
    fprintf(stderr, "%s\n", "Closing a SQLite3 Connection!");
    #endif

    return 0;
}

int ldbi_cursor_get_count(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Cursor* obj = lua_touserdata(L, 1);
    
    lua_pushinteger(L, obj->n_cols);
    lua_pushinteger(L, -1);
    
    return 2;
}

int ldbi_cursor_get_column_names(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Cursor* obj = lua_touserdata(L, 1);

    lua_newtable(L);

    int i;
    for(i = 0; i < obj->n_cols; ++i)
    {
        lua_pushstring(L, sqlite3_column_name(obj->pointer, i));
        lua_rawseti(L, 2, i + 1);
    }

    return 1;
}

int ldbi_cursor_get_column_types(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Cursor* obj = lua_touserdata(L, 1);

    lua_newtable(L);

    int i;
    for(i = 0; i < obj->n_cols; ++i)
    {
        int type = sqlite3_column_type(obj->pointer, i);
        
        switch(type)
        {
            case SQLITE_BLOB: lua_pushstring(L, "BLOB"); break;
            case SQLITE_INTEGER: lua_pushstring(L, "INTEGER"); break;
            case SQLITE_FLOAT: lua_pushstring(L, "FLOAT"); break;
            case SQLITE_NULL: lua_pushstring(L, "NULL"); break;
            default: lua_pushstring(L, "TEXT");
        }
        
        lua_rawseti(L, 2, i + 1);
    }
        
    return 1;
}

int ldbi_cursor_fetch(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    luaL_checktype(L, 2, LUA_TTABLE);

    Cursor* obj = lua_touserdata(L, 1);
    
    /* Get the next row */
    int res = sqlite3_step(obj->pointer);

    /* There are more rows? */
    if(res != SQLITE_DONE && res == SQLITE_ROW)
    {
        /* Push all the fields */
        int i;
        for(i = 0; i < obj->n_cols; ++i)
        {
            int type = sqlite3_column_type(obj->pointer, i);
            
            if(type == SQLITE_BLOB)
            {
                int size = sqlite3_column_bytes(obj->pointer, i);
                const void* val = sqlite3_column_blob(obj->pointer, i);
                lua_pushlstring(L, (const char*)val, size);
            }
            else
            {
                const unsigned char* val = sqlite3_column_text(obj->pointer, i);
                lua_pushstring(L, (const char*)val);
            }
            
            lua_rawseti(L, 2, i + 1);
        }
    }
    else
    {
        /* Consistency */
        lua_pushnil(L);
    }

    return 1;
}

int ldbi_cursor_close(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TUSERDATA);
    Cursor* obj = lua_touserdata(L, 1);
    close_cursor(obj);
    
    return 0;
}

/* Interface */

static const struct luaL_Reg env [] =
{
    {"connect", ldbi_env_connect},
    {NULL, NULL}
};

static const struct luaL_Reg conn [] =
{
    {"execute", ldbi_conn_execute},
    {"version", ldbi_conn_version},
    {"escape", ldbi_conn_escape},
    {"close", ldbi_conn_close},
    {NULL, NULL}
};

static const struct luaL_Reg cursor [] =
{
    {"getCount", ldbi_cursor_get_count},
    {"getColumnNames", ldbi_cursor_get_column_names},
    {"getColumnTypes", ldbi_cursor_get_column_types},
    {"fetch", ldbi_cursor_fetch},
    {"close", ldbi_cursor_close},
    {NULL, NULL}
};

int luaopen_ldbi__sqlite(lua_State *L)
{
    /* Register the metatables */
    ldbi_push_metatable(L, "__mtSqLiTeCoNn", conn, ldbi_conn_close, priv_conn_tostring);
    ldbi_push_metatable(L, "__mtSqLiTeCuRsOr", cursor, ldbi_cursor_close, priv_cursor_tostring);
    
    /* Register the basic functions */
    luaL_setfuncs(L, env, 0);

    return 1;
}
