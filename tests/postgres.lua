#! /usr/bin/lua5.1

local postgres = require('ldbi.postgres')

local conn, err, msg = postgres.connect{
    ['host']        = 'localhost',
    ['dbname']      = 'teste',
    ['user']        = 'kknd',
    ['password']    = '',
    ['timeout']     = 5
}

if not conn then
    print(msg)
    os.exit(1)
end

local n = 100
local par = [[hello ' \ "worl\dṕ]]

conn:execute([[CREATE TEMP TABLE testt
(
    "id" serial primary key,
    "name" varchar(20),
    "value" integer,
    "other" varchar(20),
    "flag" boolean,
    "large" decimal(20,10)
)]])

for i = 1, n do
    assert(conn:execute([[INSERT INTO testt("name", "value", "other", "flag", "large")
    VALUES ($1, $2, $3, $4, $5)]], 5, {par, i * i, nil, true, '9999.99999'}) == 1)
end

for i = n + 1, 2 * n do
    assert(conn:execute(string.format([[INSERT INTO testt("name", "value", "other", "flag", "large")
        VALUES ('%s', '%i', NULL, 't', '9999.99999')]],
        conn:escape(par), i * i)) == 1)
end

local cur, code, msg = conn:execute([[SELECT "id", "name", "value", "other", "flag", "large" FROM testt]])

local row = cur:fetch({})
local names = cur:getColumnNames()
assert(names[1] == 'id')
assert(names[2] == 'name')
assert(names[3] == 'value')
assert(names[4] == 'other')
assert(names[5] == 'flag')
assert(names[6] == 'large')

while row do
    local ncols, nrows = cur:getCount()
    assert(ncols == 6)
    assert(nrows == n * 2)
    local i = tonumber(row[1])
    assert(row[2] == par)
    assert(row[3] == tostring(i * i))
    assert(row[4] == nil)
    assert(row[5] == 't')
    assert(row[6] == '9999.9999900000')
    row = cur:fetch(row)
end

assert(conn:execute([[DELETE FROM testt WHERE id <= 100]]) == 100)

cur:close()
conn:close()

print('End.')

-- Pause to analyse memory consuption
if arg[1] == 'mem' then
    io.read('*n')
end
