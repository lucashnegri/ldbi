#! /usr/bin/lua5.1

local sqlite = require('ldbi.sqlite')

local conn, err, msg = sqlite.connect({
    ['dbname'] = ':memory:',
})

if not conn then
    print(msg)
    os.exit(1)
end

local n = 100
local par = [[hello ' \ "worl\dṕ]]

conn:execute([[CREATE TABLE testt
(
    "id" INTEGER PRIMARY KEY,
    "name" TEXT,
    "value" INTEGER,
    "other" TEXT,
    "flag" TEXT,
    "large" TEXT
)]])

for i = 1, n do
    assert(conn:execute([[INSERT INTO testt("name", "value", "other", "flag", "large")
        VALUES ($1, $2, $3, $4, $5)]], 5, {par, i * i, nil, true, '9999.99999'}) == 1)
end

for i = n + 1, 2 * n do
    assert(conn:execute(string.format([[INSERT INTO testt("name", "value", "other", "flag", "large")
        VALUES ('%s', '%i', NULL, 't', '9999.99999')]],
        conn:escape(par), i * i)) == 1)
end

local cur, code, msg = conn:execute([[SELECT "id", "name", "value", "other", "flag", "large" FROM testt]])

local row = cur:fetch({})
local names = cur:getColumnNames()
assert(names[1] == 'id')
assert(names[2] == 'name')
assert(names[3] == 'value')
assert(names[4] == 'other')
assert(names[5] == 'flag')
assert(names[6] == 'large')

while row do
    local ncols, nrows = cur:getCount()
    assert(ncols == 6)
    local i = tonumber(row[1])
    assert(row[2] == par)
    assert(row[3] == tostring(i * i))
    assert(row[4] == nil)
    assert(row[5] == 't')
    assert(row[6] == '9999.99999')
    row = cur:fetch(row)
end

assert(conn:execute([[DELETE FROM testt WHERE id <= 100]]) == 100)
cur:close()

-- binary data
conn:execute([[CREATE TABLE testbin
(
    "data" BLOB
)]])

local data = 'this is\0atest'
local bin  = sqlite.binary(data)

assert( conn:execute([[INSERT INTO testbin("data") VALUES ($1)]], 1, {bin}) )

local cur = conn:execute( [[SELECT * FROM testbin]] )
local res = cur:fetch{}
assert(res[1] == data)
cur:close()

conn:close()
print('End.')

-- Pause to analyse memory consuption
if arg[1] == 'mem' then
    io.read('*n')
end
