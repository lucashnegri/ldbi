ldbi
====

Info
----

ldbi is a set of Lua bindings to database access libraries.
Currently supports Postgresql and SQLite with Lua 5.1 / 5.2 / LuaJIT.

Dependencies
------------

* libpq, with development files (for Postgres);
* sqlite, with development files (for SQLite);
* Lua 5.1 / 5.2 / LuaJIT, with development files;

Install
-------

Edit the Makefile to suit your needs, and run

    $ make [DEBUG=1]
    # make install [DESTDIR=/usr/local]

ldbi is portable and has been tested with Arch Linux (x86/x64), Fedora 9 (x86),
Debian Lenny (x64) and MS-Windows XP (x86).

Usage
-----

Its similar to the luasql library. See _tests_ for some examples.

Contact
-------

Lucas Hermann Negri - <lucashnegri@gmail.com>
http://oproj.tuxfamily.org
