# Compiler and flags
CC=gcc
PKG_PACKAGES=lua
CFLAGS=-c -pipe -Wall `pkg-config --cflags $(PKG_PACKAGES)` -fPIC
LDFLAGS=-shared `pkg-config --libs $(PKG_PACKAGES)`
OUTDIR=lua/5.1/

ifdef DEBUG
	CFLAGS+=-g -O0 -DIDEBUG
else
	CFLAGS+=-O2
endif

# Platform dependent
RM=rm -rf
CP=cp
MKDIR=mkdir -p
DESTDIR=/usr/local
NAME_POSTGRES=_postgres.so
NAME_SQLITE=_sqlite.so

# Rules

.PHONY: all
all: $(NAME_POSTGRES) $(NAME_SQLITE)

src/ldbi.o: src/ldbi.c src/ldbi.h
	$(CC) $(CFLAGS) src/ldbi.c -o src/ldbi.o

$(NAME_POSTGRES): src/postgres.c src/ldbi.o
	$(CC) $(CFLAGS) src/postgres.c -o src/postgres.o
	$(CC) src/postgres.o src/ldbi.o -o $(NAME_POSTGRES) $(LDFLAGS) -lpq
	
$(NAME_SQLITE): src/sqlite.c src/ldbi.o
	$(CC) $(CFLAGS) src/sqlite.c -o src/sqlite.o
	$(CC) src/sqlite.o src/ldbi.o -o $(NAME_SQLITE) $(LDFLAGS) -lsqlite3

.PHONY: install-base
install-base:
	$(MKDIR) $(DESTDIR)/lib/$(OUTDIR)/ldbi
	$(MKDIR) $(DESTDIR)/share/$(OUTDIR)/ldbi
	
.PHONY: install-postgres
install-postgres: $(NAME_POSTGRES) install-base
	$(CP) $(NAME_POSTGRES) $(DESTDIR)/lib/$(OUTDIR)/ldbi
	$(CP) src/postgres.lua $(DESTDIR)/share/$(OUTDIR)/ldbi/postgres.lua 
	
.PHONY: install-sqlite
install-sqlite: $(NAME_SQLITE) install-base
	$(CP) $(NAME_SQLITE) $(DESTDIR)/lib/$(OUTDIR)/ldbi
	$(CP) src/sqlite.lua $(DESTDIR)/share/$(OUTDIR)/ldbi/sqlite.lua

.PHONY: install
install: install-postgres install-sqlite

.PHONY: clean
clean:
	$(RM) src/*.o $(NAME_POSTGRES) $(NAME_SQLITE)

.PHONY: help
help:
	@echo Options:
	@echo
	@echo AMD64=1 to build for 64 bits
	@echo DEBUG=1 to enable debug mode
